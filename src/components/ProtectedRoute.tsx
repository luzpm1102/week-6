import React, { useEffect, useState } from 'react';
import { useLocalStorage } from '../hooks/useLocalStorage';
import { Login } from '../screens/Login';
import { UserWelcome } from '../screens/UserWelcome';

export const ProtectedRoute = (): JSX.Element => {
  const { getItem } = useLocalStorage();
  const [token, setToken] = useState(getItem('token'));
  useEffect(() => {
    setToken(getItem('token'));
  }, []);

  const [user] = useState(getItem('user'));
  if (!token) {
    return <Login token={token} />;
  }
  return <UserWelcome userName={user} />;
};
