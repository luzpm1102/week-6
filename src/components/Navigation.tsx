import React from 'react';
import { Route, Routes, BrowserRouter } from 'react-router-dom';
import { GameDetails } from '../screens/GameDetails';
import { GameList } from '../screens/GameList';
import { HomePage } from '../screens/HomePage';
import { SearchPage } from '../screens/SearchPage';
import { ProtectedRoute } from './ProtectedRoute';
export enum paths {
  home = '/',
  gameList = '/Games',
  login = '/authentication',
  gameDetails = '/GameDetail/:id',
  search = '/Search/:name',
}
export const Navigation = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={paths.home} element={<HomePage />} />
        <Route path={paths.gameList} element={<GameList />} />
        <Route path={paths.login} element={<ProtectedRoute />} />
        <Route path={paths.gameDetails} element={<GameDetails />} />
        <Route path={paths.search} element={<SearchPage />} />
      </Routes>
    </BrowserRouter>
  );
};
