import React, { useMemo, useState } from 'react';
import { GAMES } from '../helpers/constants';
import { checkInput, handleChange, setInitialState } from '../helpers/helpers';
import { useApi } from '../hooks/useApi';
import { useLocalStorage } from '../hooks/useLocalStorage';
import { usePagination } from '../hooks/usePagination';
import { post } from '../services/post';
import { Comment } from './Comment';
import Pagination from './Pagination';
import { IComment } from '../interfaces/interfaces';
import { Loading } from './Loading';
interface Props {
  id: number;
}

export const Comments = ({ id }: Props) => {
  const uri = `${GAMES}${id}/comments?_limit=-1`;
  const url = `${GAMES}${id}/comment`;
  const { data, loading } = useApi(uri);
  const { getItem } = useLocalStorage();
  const [token, setToken] = useState(getItem('token'));
  const dataComment = data as IComment[];

  const [body, setBody] = useState('');
  const [error, setError] = useState('');
  const commentsLimit = 6;

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(commentsLimit);

  const comments = useMemo(
    () => displayComments(dataComment, indexOfTheFirstItem, indexOfTheLastItem),
    [indexOfTheFirstItem, indexOfTheLastItem, dataComment]
  );

  const sendComment = async () => {
    if (!checkInput('Comment', body, setError)) {
      return;
    }
    const comment = {
      body: body,
    };
    try {
      await post(url, comment, token);
      alert('Commented');
      setInitialState(setBody);
      setToken(getItem('token'));
    } catch (error) {
      alert(error);
    }
  };

  return (
    <>
      {loading.current && <Loading />}
      {!loading.current && (
        <div className='game-comments__comment'>
          {comments}
          <Pagination
            itemsPerPage={itemsPerPage}
            totalpages={
              dataComment.filter(
                (comment: IComment) => comment.body.trim() !== ''
              ).length
            }
            paginate={paginate}
            currentPage={currentPage}
          />
        </div>
      )}
      {!token ? (
        <div className='game-comments__title'>
          <h3>Log in to comment</h3>
        </div>
      ) : (
        <div className='game-comments__user-comment'>
          <>{error && <small className='error'>{error}</small>}</>
          <input
            type='text'
            value={body}
            onChange={(e) => handleChange(e, setBody)}
            className='game-comments__input'
          />

          <button
            className='game-comments__button button-primary'
            onClick={sendComment}
          >
            Send
          </button>
        </div>
      )}
    </>
  );
};

const displayComments = (
  data: IComment[],
  indexOfTheFirstItem: number,
  indexOfTheLastItem: number
) => {
  let resultFiltered;
  if (data) {
    const result = data
      .sort((a: IComment, b: IComment) => {
        var dateA = new Date(a.created_at);
        var dateB = new Date(b.created_at);
        return +dateB - +dateA;
      })
      .filter((comment: IComment) => comment.body.trim() != '');

    resultFiltered = result
      .slice(indexOfTheFirstItem, indexOfTheLastItem)
      .map((comment: IComment) => (
        <Comment
          comment={comment.body}
          username={comment.user.username}
          key={comment.id}
        />
      ));
  }

  return resultFiltered;
};
