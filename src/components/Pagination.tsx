import React from 'react';
import '../styles/pagination.scss';
interface Props {
  itemsPerPage: number;
  totalpages: number;
  paginate: (pageNumber: number) => void;
  currentPage: number;
}

const Pagination = ({
  itemsPerPage,
  totalpages,
  paginate,
  currentPage,
}: Props) => {
  const pageNumbers = [];
  const numbersLimit = [1, 2, 3, 4, 5];

  for (let i = 1; i <= Math.ceil(totalpages / itemsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <nav className='pagination-container'>
      <ul className='pagination-list'>
        <li className='pagination-list__item '>
          <a
            className={
              currentPage === 1
                ? 'pagination-list__link-normal'
                : 'pagination-list__link'
            }
            onClick={() =>
              currentPage === 1
                ? paginate(currentPage)
                : paginate(currentPage - 1)
            }
          >
            {' '}
            {'<'}
          </a>
        </li>
        {pageNumbers.length < 5
          ? pageNumbers.map((number) => (
              <li key={number} className='pagination-list__item '>
                <a
                  className={
                    currentPage === number
                      ? 'pagination-list__link pagination-list__link--active'
                      : 'pagination-list__link '
                  }
                  onClick={() => {
                    paginate(number);
                  }}
                >
                  {number}
                </a>
              </li>
            ))
          : numbersLimit.map((number) => (
              <li key={number} className='pagination-list__item '>
                <a
                  className={
                    currentPage === number
                      ? 'pagination-list__link pagination-list__link--active'
                      : 'pagination-list__link '
                  }
                  onClick={() => {
                    paginate(number);
                  }}
                >
                  {number}
                </a>
              </li>
            ))}
        <li className='pagination-list__item '>
          <a
            className={
              currentPage === pageNumbers.length
                ? 'pagination-list__link-normal'
                : 'pagination-list__link'
            }
            onClick={() =>
              currentPage === pageNumbers.length
                ? paginate(pageNumbers.length)
                : paginate(currentPage + 1)
            }
          >
            {' '}
            {'>'}
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default Pagination;
