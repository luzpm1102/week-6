import React from 'react';
import { MemoizedFooter } from './Footer';
import { NavBar } from './NavBar';
interface Props {
  children: React.ReactNode;
}
export const Layout = ({ children }: Props) => {
  return (
    <>
      <div className='home-container'>
        <NavBar />
        <div className='main-container'>{children}</div>

        <MemoizedFooter />
      </div>
    </>
  );
};
