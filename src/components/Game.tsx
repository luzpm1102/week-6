import React from 'react';
import { Link } from 'react-router-dom';
import { defaultImage } from '../helpers/constants';
import '../styles/games.scss';
interface Props {
  image: string;
  name: string;
  price: string;
  id: number;
}

const Game = ({ image, name, price, id }: Props) => {
  return (
    <Link to={`/GameDetail/${id}`}>
      <div className='card-container'>
        <div className='card-container__header'>
          <img src={image ? image : defaultImage} alt='' />
        </div>
        <div className='card-container__body'>
          <h4>{name}</h4>
        </div>
        <div className='card-container__price'>
          <div className='card-container__price__price-text'>{'$' + price}</div>
        </div>
      </div>
    </Link>
  );
};

export default Game;
