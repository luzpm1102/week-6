import React from 'react';
import { useApi } from '../hooks/useApi';
import Game from './Game';
import Pagination from './Pagination';
import '../styles/games.scss';
import { usePagination } from '../hooks/usePagination';
import { IGame } from '../interfaces/interfaces';
import { Loading } from './Loading';
interface Props {
  uri: string;
}
export const Games = ({ uri }: Props) => {
  const { data, loading } = useApi(uri);
  const dataGames = data ? (data as IGame[]) : [];
  const gamesLimit = 6;

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(gamesLimit);

  const displayGames = dataGames
    .slice(indexOfTheFirstItem, indexOfTheLastItem)
    .map(({ name, price, id, cover_art }: IGame) => (
      <Game
        key={id}
        image={cover_art ? cover_art.formats.small.url : cover_art}
        name={name}
        price={price}
        id={id}
      />
    ));
  return (
    <>
      {loading.current && <Loading />}
      {dataGames.length > 0 && !loading.current && (
        <>
          <div className='cards-container'>{displayGames}</div>
          <Pagination
            itemsPerPage={itemsPerPage}
            totalpages={dataGames.length}
            paginate={paginate}
            currentPage={currentPage}
          />
        </>
      )}
    </>
  );
};
