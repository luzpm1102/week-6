import React from 'react';
import '../styles/games.scss';

interface Props {
  comment: string;
  username: string;
}

export const Comment = ({ comment, username }: Props) => {
  return (
    <div className='comment-container'>
      <div className='comment__username'>
        <h5>{username}</h5>
      </div>
      <hr />
      <div className='comment__body'>
        <p>{comment}</p>
      </div>
    </div>
  );
};
