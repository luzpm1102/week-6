import React from 'react';
import { Link } from 'react-router-dom';

export const GoBackButton = () => {
  return (
    <div className='container--center'>
      <Link to={'/Games'}>
        <button className='info__button button-primary'>Go Back</button>
      </Link>
    </div>
  );
};
