import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import logo from '../assets/img/game.png';
import { useLocalStorage } from '../hooks/useLocalStorage';
import '../styles/navbar.scss';
import { paths } from './Navigation';

export const NavBar = () => {
  const { getItem } = useLocalStorage();
  const [token, setToken] = useState(getItem('token'));

  useEffect(() => {
    setToken(getItem('token'));
  }, [token]);

  return (
    <header>
      <div className='navbar'>
        <img className='navbar__logo' src={logo} alt='logo' />
        <Link to={paths.home} className='navbar--link'>
          <div className='navbar__title'>
            <h2>Game Store</h2>
          </div>
        </Link>
        <Link to={paths.gameList} className='navbar--link'>
          <div className='navbar__title'>
            <h3 className='navbar__title--position'>Game List</h3>
          </div>
        </Link>
        <Link to={paths.login} className='navbar--link'>
          <div className='navbar__title'>
            {!token ? (
              <h3 className='navbar__title--position'>Login</h3>
            ) : (
              <h3 className='navbar__title--position'>Logout</h3>
            )}
          </div>
        </Link>
      </div>
    </header>
  );
};
