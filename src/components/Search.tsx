import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { handleChange } from '../helpers/helpers';

export const Search = () => {
  const [word, setWord] = useState('');
  const navigate = useNavigate();
  const [error, setError] = useState('');

  const returnSearch = () => {
    if (!word) {
      setError('search word required');
      return;
    }
    return navigate(`/Search/${word}`);
  };

  return (
    <div className='search'>
      <input
        className='search__input'
        type='text'
        value={word}
        onChange={(e) => handleChange(e, setWord)}
      />
      <button className='button-primary' onClick={returnSearch}>
        search
      </button>
      {error && <small className='error'> {error}</small>}
    </div>
  );
};
