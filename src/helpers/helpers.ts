import React from 'react';

export const handleChange = (
  e: React.ChangeEvent<HTMLInputElement>,
  set: React.Dispatch<React.SetStateAction<string>>
) => {
  set(e.currentTarget.value);
};

export const checkInput = (
  inputName: string,
  value: string,
  set: React.Dispatch<React.SetStateAction<string>>
) => {
  if (value === '') {
    set(inputName + ' required');
    return false;
  }
  return true;
};

export const setInitialState = (
  set: React.Dispatch<React.SetStateAction<string>>
) => {
  set('');
};

export const sort = (a: string, b: string) => {
  const dateA = new Date(a);
  const dateB = new Date(b);
  const result = +dateA - +dateB;
  return result;
};
