import { BASE_URL } from '../helpers/constants';

export const post = async (uri, body, token) => {
  let headers = {
    'Content-type': 'application/json',
  };
  if (token) {
    headers = {
      'Content-type': 'application/json',
      Authorization: `Bearer ${token}`,
    };
  }

  const data = await fetch(`${BASE_URL}${uri}`, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(body),
  });
  const jsonData = await data.json();

  return jsonData;
};
