import React from 'react';
import { useParams } from 'react-router-dom';
import { Games } from '../components/Games';
import { GoBackButton } from '../components/GoBackButton';
import { Layout } from '../components/Layout';
import { Search } from '../components/Search';
import { GAMES } from '../helpers/constants';

export const SearchPage = () => {
  const { name } = useParams();

  return (
    <Layout>
      <Search />
      <Games uri={`${GAMES}?name_contains=${name}`} />
      <GoBackButton />
    </Layout>
  );
};
