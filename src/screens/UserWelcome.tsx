import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Layout } from '../components/Layout';
import { useLocalStorage } from '../hooks/useLocalStorage';
interface Props {
  userName: string | null;
}

export const UserWelcome = ({ userName }: Props) => {
  const navigate = useNavigate();
  const { deleteItem } = useLocalStorage();
  const handleLogout = () => {
    deleteItem('token');
    navigate('/');
  };
  return (
    <Layout>
      <div className='user-info'>
        <h3>Welcome {userName}</h3>

        <button
          onClick={handleLogout}
          className='user-info__button button-primary'
        >
          Log Out
        </button>
      </div>
    </Layout>
  );
};
