import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { Comments } from '../components/Comments';
import { Layout } from '../components/Layout';
import { defaultImage, GAMES } from '../helpers/constants';
import { useApi } from '../hooks/useApi';
import { IGame } from '../interfaces/interfaces';
import { Loading } from '../components/Loading';
import { GoBackButton } from '../components/GoBackButton';

export const GameDetails = () => {
  const { id } = useParams();
  const { data, loading } = useApi(`${GAMES}${id}`);

  const game = data as IGame;

  return (
    <Layout>
      {loading.current && <Loading />}
      {game && !loading.current && game.id && (
        <div className='info-container'>
          <div className='game-details-container'>
            <div className='game-details__image-container'>
              <img
                src={
                  game.cover_art
                    ? game.cover_art.formats.small.url
                    : defaultImage
                }
                alt={game.name}
                className='image-container__img'
              />
            </div>
            <div className='game-details__body'>
              <h3>{game.name}</h3>
              <p>{game.name}</p>
              <h4>
                Publisher:{' '}
                {game.publishers.length > 0
                  ? game.publishers[0].name
                  : 'No publisher'}
              </h4>
              <h4>Genre: {game.genre.name}</h4>
              <button
                className='game-details__price__button button-primary'
                onClick={() => alert('You bougth ' + game.name)}
              >
                <div className='game-details__price'>
                  <h4 className='game-details__price-text'>
                    {'Buy this game $' + game.price}
                  </h4>
                </div>
              </button>
            </div>
          </div>

          <div className='game-comments'>
            <h3 className='game-comments__title'>Comments section</h3>
            {game.id && <Comments id={game.id} />}
          </div>
          <GoBackButton />
        </div>
      )}
    </Layout>
  );
};
