import React from 'react';
import { Games } from '../components/Games';
import { Layout } from '../components/Layout';
import { Search } from '../components/Search';
import { GAMES } from '../helpers/constants';

export const GameList = () => {
  return (
    <Layout>
      <div className='main-container'>
        <div className='header-title-container'>
          <h1 className='header-title'>Games available</h1>
        </div>
        <Search />
        <Games uri={GAMES} />
      </div>
    </Layout>
  );
};
