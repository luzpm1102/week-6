import React, { MutableRefObject, useEffect, useRef, useState } from 'react';
import { Layout } from '../components/Layout';
import { checkInput, handleChange, setInitialState } from '../helpers/helpers';
import { useAuth } from '../hooks/useAuth';
import { useLocalStorage } from '../hooks/useLocalStorage';
import '../styles/login.scss';
import { UserWelcome } from './UserWelcome';
interface Props {
  token: string | null;
}

export const Login = ({ token }: Props) => {
  const [user, setUser] = useState('');
  const [pass, setPass] = useState('');
  const { getItem } = useLocalStorage();
  const userCompleteName = getItem('user');
  const { auth } = useAuth();
  const [error, setError] = useState('');

  const usernameInput = useRef() as MutableRefObject<HTMLInputElement>;
  useEffect(() => {
    if (!token) {
      usernameInput.current.focus();
    }
  }, [token]);

  const authentication = async (user: string, pass: string) => {
    if (
      !checkInput('user', user, setError) ||
      !checkInput('password', pass, setError)
    ) {
      setError('Username and Password required');
      return;
    }
    try {
      const response = await auth(user, pass);
      if (response.statusCode >= 400) {
        throw new Error(response.error);
      }
      setInitialState(setUser);
      setInitialState(setPass);
      window.location.reload();
    } catch {
      setError('Password or username incorrect');
    }
  };

  return (
    <Layout>
      {!token ? (
        <div className='form-container'>
          <div className='form'>
            <div className='form-body'>
              {error && <small className='error--secondary'>{error}</small>}

              <label htmlFor='username' className='form-label'>
                Username
              </label>
              <input
                ref={usernameInput}
                type='text'
                id='username'
                className='form-input'
                value={user}
                onChange={(e) => handleChange(e, setUser)}
              />
              <label htmlFor='password' className='form-label'>
                Password
              </label>
              <input
                type='password'
                id='password'
                className='form-input'
                value={pass}
                onChange={(e) => handleChange(e, setPass)}
              />
              <button
                className='form-body__button'
                onClick={() => authentication(user, pass)}
              >
                Login
              </button>
            </div>
          </div>
        </div>
      ) : (
        <UserWelcome userName={userCompleteName} />
      )}
    </Layout>
  );
};
