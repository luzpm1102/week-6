export const useLocalStorage = () => {
  const getItem = (key: string) => {
    const item = localStorage.getItem(key);
    return item;
  };

  const setItem = (key: string, value: string) => {
    localStorage.setItem(key, value);
    return 'Item saved';
  };
  const deleteItem = (key: string) => {
    localStorage.removeItem(key);
    return 'Item Deleted';
  };

  return {
    getItem,
    setItem,
    deleteItem,
  };
};
