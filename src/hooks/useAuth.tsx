import { AUTH } from '../helpers/constants';
import { post } from '../services/post';
import { useLocalStorage } from './useLocalStorage';

export const useAuth = () => {
  const { setItem } = useLocalStorage();
  const auth = async (identifier: string, password: string) => {
    const credentials = {
      identifier: identifier,
      password: password,
    };

    const response = await post(AUTH, credentials);
    const token = await response.jwt;

    const user = `${response.user.firstName} ${response.user.lastName}`;

    setItem('token', token);
    setItem('user', user);
    return response;
  };

  return {
    auth,
  };
};
