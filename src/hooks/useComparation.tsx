import { useState } from 'react';
import usePrevious from './usePrevious';

const useComparation = (state: string) => {
  const [isEqual, setIsEqual] = useState(false);
  const ref = usePrevious(state);

  if (ref === state) {
    setIsEqual(true);
  }
  return isEqual;
};

export default useComparation;
