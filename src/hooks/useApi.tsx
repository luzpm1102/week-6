import { useEffect, useRef, useState } from 'react';
import { BASE_URL } from '../helpers/constants';
import { IGame, IComment } from '../interfaces/interfaces';
export const useApi = (uri: string) => {
  const [data, setData] = useState<IGame[] | IComment[] | IGame | IComment>();
  const loading = useRef(true);

  useEffect(() => {
    const getData = async () => {
      const result = await fetch(`${BASE_URL}${uri}`)
        .then((res) => res.json())
        .catch((error) => alert(error));

      loading.current = false;

      setData(result);
    };

    getData();
  }, [uri]);

  return {
    data,
    loading,
  };
};
